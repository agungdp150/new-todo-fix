import axios from "axios"
import { LOGIN_SUCCESS, LOGIN_FAIL} from "../types"

// Import Helpers
import setToken from "../helpers/setToken.js"


export const login = LoginForm => async dispatch => {
  if (sessionStorage.token) {
    setToken(sessionStorage.token);
  }
  try {
    const response = await axios({
    method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
      url: 'https://ga-todolist-api.herokuapp.com/api/user/login',
      data: LoginForm,
    });
    console.log(response);
    // console.log('token :  ', response.data.token)
    dispatch({
      type: LOGIN_SUCCESS,
      payload: response.data
    });
  } catch (error) {
    console.log(error.response.data);
    dispatch({
      type: LOGIN_FAIL
    })
  }
}