import React, {Component} from 'react';
import {Provider} from "react-redux"
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import store from "./store"

// Import Components
import LoginPage from "./pages/LoginPage"
import Register from "./pages/Register"
import AfterLogin from "./pages/AfterLogin"
import ForgotPassword from "./pages/ForgotPassword"

// Import Style
import './App.css';

class App extends Component {
  render () {
    return (
      <Provider store={store}>
      <Router>
        <div>
          <Switch>
            <Route path="/" exact component={LoginPage}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/afterlogin" exact component={AfterLogin}/>
            <Route path="/forgot" exact component={ForgotPassword}/>
          </Switch>
       </div>
      </Router>
      </Provider>
    );
  }
}

export default App;
