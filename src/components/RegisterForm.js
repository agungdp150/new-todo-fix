import React, { Component } from 'react'
import { Link } from "react-router-dom" 
import {connect} from "react-redux"
import {register} from "../actions/registerAction"
// import {createBrowserHistory} from "history"


class RegisterForm extends Component {
  constructor(props) {
    super (props);

    this.state = {
      name : "",
      email : "",
      password : ""
    };
  };

  // handle Change
  handleChange = (e) => {
    this.setState ({
      [e.target.name] : e.target.value
    })
  }

  // Handle input submit
  handleSubmit = e => {
    e.preventDefault()
    // console.log ("ooke")
    // const history = createBrowserHistory()

    const formRegis = {
      name : this.state.name,
      email : this.state.email,
      password : this.state.password
    }
    // console.log (formRegis)
    // console.log (this.context.history.push("/"))
    this.props.register(formRegis)
      this.props.history.push("/")
  }

  render() {

    const {name, email, password} = this.state;

    return (
      <div>
        <div className="in-sign">
          <i className="fas fa-chevron-down fa-5x"/>
          <h1>Welcome ToDo App</h1>
          <p>Create your account now and make your plan will comfortable</p>
        </div>
        <div className="f-container">
          <h1>Sign Up ToDo App</h1>
          <div className="f-icon">
            <p><i className='fab fa-facebook-square fa-2x' /></p>
            <p><i className='fab fa-google-plus-square fa-2x' /></p>
            <p><i className='fab fa-linkedin fa-2x' /></p>
          </div>
          <p className="p-con1" > login using your account</p>
          <div className="f-form1">
            <form onSubmit={this.handleSubmit}>
              <input
                type="text"
                placeholder="name"
                name="name"
                value = {name}
                onChange={this.handleChange}
              />
              <input 
                type="email"
                placeholder="todo@todomail.com"
                name = "email"
                value = {email}
                onChange={this.handleChange}
              />
              <input
                type="password"
                placeholder="password"
                name = "password"
                value = {password}
                onChange = {this.handleChange}
              />
              <div className='btn-name'>
                <button type='submit'>
                  Sign Up
                </button>
                <div className='btn-re re-1'>
                  <Link to='/'>
                      <p>Login</p> 
                  </Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    isAuthencated : state.registerState.isAuthencated
  }
}


export default connect( 
  mapStateToProps,
  {register},
) (RegisterForm);
