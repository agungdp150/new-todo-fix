import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import {connect} from "react-redux";
import {login} from "../actions/loginAction.js"

class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listUser : [],
      email: "",
      password: ""
    };
  }

  // Get data user from API
  getData = async() => {
    await  axios ({
      method : "GET",
      url : `https://ga-todolist-api.herokuapp.com/api/user`,
      headers : {
        "Content-Type" : "application/json"
    }    
  })
  .then(response => {
    // console.log (response.data.data)
    this.setState({listUser : response.data.data})
  })
}

componentDidMount = () => {
  this.getData()
}


  // Handle Change
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  // handle input login submit
  handleSubmit = e => {
    e.preventDefault();

    // const acc = this.state.listUser.map(userList => {
    //   return (userList.email)
    //   console.log(userList.email)
    // })

    const formData = {
      email : this.state.email,
      password : this.state.password
    }
    // console.log (formData)
    // console.log (formData.email)
    this.props.login(formData);

    if (formData.email) {
      this.props.history.push("./afterlogin")
    } else {
      alert("Something went wrong!")
    }
  }


  render() {
    const { email, password } = this.state;

    return (
      <div>
        <div className='in-sign'>
          <i className='fas fa-chevron-down fa-5x' />
          <h1>Welcome ToDo App</h1>
          <p>ToDo List App to simplify your task management everyday</p>
        </div>
        <div className='f-container'>
          <h1>Sign In to ToDo App</h1>
          <div className='f-icon'>
            <p><i className='fab fa-facebook-square fa-2x' /></p>
            <p><i className='fab fa-google-plus-square fa-2x' /></p>
            <p><i className='fab fa-linkedin fa-2x' /></p>
          </div>
          <p className='p-con1'>or login using your account</p>
          <div className='f-form1'>
            <form onSubmit={this.handleSubmit}>
              <input
                type='email'
                placeholder='todo@email.com'
                name='email'
                value={email}
                onChange={this.handleChange}
              />
              <br />
              <input
                type='password'
                placeholder='password'
                name='password'
                required
                value={password}
                onChange={this.handleChange}
              />
              <br />
              <div className='btn-name'>
                <button type='submit'>Sign In</button>
                <div className='btn-re'>
                  <Link to='/forgot'>
                    <p>Forgot Password ?</p>
                  </Link>
                  <p>didn't have account yet? register</p>
                  <Link to='/register'>
                    <p>
                      <span>here...</span>
                    </p>
                  </Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthencated : state.loginState.isAuthencated
  }
}


export default connect(
  mapStateToProps,
  {login}
) (LoginForm);
