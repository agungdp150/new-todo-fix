import React, { Component } from 'react';
import {Link} from "react-router-dom";


class ForgotComp extends Component {
  render() {
    return (
    <div className="re-container">
      <div className="re-font">
        <h1><i class="fas fa-key fa-3x"></i>?</h1>
        <h3>Yo! Forgot Password ?</h3>
        <p>No Worries! Enter your email and we send you a reset.</p>
      </div>
      <div className="re-form">
        <form>
        <input type="email" required placeholder="todo@email.com"></input><br/>
        <button>Send Request</button>
        </form>
      </div>
      <Link to="/" >
        Back to sign in...
      </Link>
    </div>
    )
  }
}


export default ForgotComp;
