import React, { Component } from 'react'
import { Link } from "react-router-dom"
// import setToken from "../helpers/setToken.js"
import axios from "axios";


class InputList extends Component {
  constructor(props) {
    super (props) ;

    this.state = {
      listTodos : [],
      token : null,
      newTitle : "",
      newTodo : "",
      newPriority : "",
      newDate : "2019-08-28T14:30:19.904Z"
    }
  }
  

  // Get Token
  // getToken = async () => {
  //   try {
  //     const token = await sessionStorage.getItem("token");
  //     if (token !== null) {
  //       this.setState({token : token});
  //       console.log (this.state.token)
  //     }
  //   } catch (error){
  //     console.log ("something went wrong" + error)
  //   } 
  // }

  // Get Data Todo
  componentDidMount(){
    this.setState({
      token: sessionStorage.getItem("token")
    })
  }


  // Change nilai State
  handleChange = (e) => {
    this.setState ({
      [e.target.name] : e.target.value
    })
  }

  // Post new todo
  handleInput = async (e) => {
    e.preventDefault()
    // console.log ("oke")
    // console.log (sessionStorage.getToken("token"))
    console.log("test")
    console.log (sessionStorage.getItem("token"))
    const body = JSON.stringify({
      title : this.state.newTitle,
      description : this.state.newTodo,
      priority : this.state.newPriority,
      dueDateTime : this.state.newDate
    })

    console.log(body)
    console.log(this.state.token)

    try {
    //  const response = await axios ({
    //     method : "POST",
    //     url : "https://ga-todolist-api.herokuapp.com/api/todo",
    //     headers : {
    //       "Content-Type" : "application/json",
    //       "Authorization" : this.state.token
    //     },body
        
    //     //  console.log (this.response.data)
    //   })
    //   console.log (response.errorMessage)
    const res = axios.post("https://ga-todolist-api.herokuapp.com/api/todo", {
      headers: {
        "Content-Type": "application/json",
        "Authorization": sessionStorage.getItem("token")
      },body
    })
    console.log(res)
    }catch(error){
      console.log(error.response.error)
    }  
  }
  
  render() {

    const {newTitle, newTodo, newPriority, token} = this.state
console.log(token)

    return (
      <div className="af-container">
        <nav>
          <div className="logo">
            <h4>TOdo</h4>
          </div>
          <ul className="nav-links">
            <Link to="/">
              <li>Sign Out</li>
            </Link>
          </ul>
        </nav>
        <form onSubmit={this.handleInput}>
          <h3>What your list?</h3>
          <input
            type = "text"
            placeholder = "name"
            name = "newTitle"
            autoComplete="off"
            value = {newTitle}
            onChange = {this.handleChange}
          /><br/>
          <input
            type = "text"
            placeholder = "priority"
            name = "newPriority"
            autoComplete="off"
            value = {newPriority}
            onChange = {this.handleChange}
          /><br/>
          <textarea
            type = "text"
            placeholder = "description"
            name = "newTodo"
            value = {newTodo}
            onChange = {this.handleChange}
          /><br/>
          <button type ="submit">+</button>
        </form>
          {/* {dataItems} */}
      </div>
    )
  }
}

export default InputList;