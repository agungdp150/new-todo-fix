import React, { Component } from 'react'

// Import Component
import RegisterForm from "../components/RegisterForm.js"

class Register extends Component {
  render() {
    return (
      <div>
        <RegisterForm {...this.props}/>
      </div>
    )
  }
}



export default Register;