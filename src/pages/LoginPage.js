import React, { Component } from 'react'

// Import Component
import LoginForm from "../components/LoginForm.js"

class LoginPage extends Component {
  render() {
    return (
      <div>
        <LoginForm {...this.props}/>
      </div>
    )
  }
}


export default LoginPage;
