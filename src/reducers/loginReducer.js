import {LOGIN_SUCCESS, LOGIN_FAIL} from "../types"

const initialState = {
  token : sessionStorage.getItem("token"),
  isAuthencated : null,
  error : [],
  loading : true
}

export default function (state = initialState, action) {
  const {type, payload} = action;

  switch (type) {
    case LOGIN_SUCCESS : 
    sessionStorage.setItem("token", payload.token)
    return {
      ...state,
      ...payload,
      isAuthencated : true,
      loading : false
    };
    case LOGIN_FAIL :
      sessionStorage.removeItem("token")
      return {
        ...state,
        isAuthencated : false,
        loading : false,
        errors : payload
      }
    default :
    return state;
  }
}