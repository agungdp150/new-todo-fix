import {REGISTER_SUCCESS, REGISTER_FAIL} from "../types"

const initialState = {
  token : sessionStorage.getItem("token"),
  isAuthencated : null,
  error : [],
  loading : true
}

export default function (state = initialState, action) {
  const {type, payload} = action;

  switch (type) {
    case REGISTER_SUCCESS : 
    sessionStorage.setItem("token", payload.token)
    return {
      ...state,
      ...payload,
      isAuthencated : true,
      loading : false
    };
    case REGISTER_FAIL :
      sessionStorage.removeItem("token")
      return {
        ...state,
        isAuthencated : false,
        loading : false,
        errors : payload
      }
    default :
    return state;
  }
}